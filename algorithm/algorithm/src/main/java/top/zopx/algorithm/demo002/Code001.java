package top.zopx.algorithm.demo002;

import java.util.Arrays;

/**
 * 链表
 *
 * @author Mr.Xie
 * @email xiezhyan@126.com
 * @date 2023/6/24 19:05
 */
public class Code001 {

    // 单链表逆序
    // next = head.next
    // head.next = pre
    // pre = head
    // head = next

    // 双链表逆序
    // next = head.next
    // head.next = pre
    // head.last = next
    // pre = head
    // head = next

    // 把给定的值删除
        // 第一个循环： 找到第一个不需要删除的节点作为返回头节点
        // 第二个循环： 判断节点的值是否与num一致

    // 单链表实现队列和栈

    // 双链表实现队列和栈

    // K个节点的组内逆序调整
    // 给一个start，找到第K个节点，返回
    // 逆序： 注意找到end.next cur != end
    // lastEnd.next = end lastEnd = end

    // 两个链表相加 (L有 S有 || L有S无 || L无S无)
    // 找到长短链表
    // a.value + b.value + carry = ?
    // value = sum % 10
    // carry = sum / 10
    // last = 最后一个节点，需要一直跟踪
    // 返回L长链表

    // 两个有序链表合并
    // 谁小谁往下


    // 两个有序数组合并
    public static int[] mergeArr(int[] arrA, int[] arrB) {
        int l1 = arrA.length;
        int l2 = arrB.length;
        int l3 = Math.min(l1, l2);

        int[] ans = new int[l1 + l2];

        int cur1 = 0;
        int cur2 = 0;
        int index = 0;
        while (cur1 <= l3 && cur2 <= l3) {
            if (arrA[cur1] <= arrB[cur2]) {
                ans[index++] = arrA[cur1];
                cur1++;
            } else {
                ans[index++] = arrB[cur2];
                cur2++;
            }
        }

        while (cur1 < l1) {
            ans[index++] = arrA[cur1];
            cur1++;
        }

        while (cur2 < l2) {
            ans[index++] = arrB[cur2];
            cur2++;
        }
        return ans;
    }



    public static void main(String[] args) {
        int[] arrA = {1, 3, 4, 5};
        int[] arrB = {2,4,5};

        System.out.println(Arrays.toString(mergeArr(arrA, arrB)));
    }
}
