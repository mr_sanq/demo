package top.zopx.algorithm.demo002;

import java.util.*;

/**
 * 二叉树
 *
 * @author Mr.Xie
 * @email xiezhyan@126.com
 * @date 2023/6/24 19:05
 */
public class Code003 {

    public static class Node {
        int val;
        Node left;
        Node right;

        Node(int val) {
            this.val = val;
        }
    }

    // 先：头 -》 左 -》 右
    // 中：左 -》 中 -》 右
    // 后：左 -》 右 -》 中
    // 递归序： 递归函数执行过程。。。
    public static void pre(Node root) {
        if (null == root) {
            return;
        }
        System.out.println(root.val);
        pre(root.left);
        pre(root.right);
    }

    public static void in(Node root) {
        if (null == root) {
            return;
        }
        in(root.left);
        System.out.println(root.val);
        in(root.right);
    }

    public static void pos(Node root) {
        if (null == root) {
            return;
        }
        pos(root.left);
        pos(root.right);
        System.out.println(root.val);
    }

    // 两个树判断是否完全相等
    public static boolean isSameTree(Node p, Node q) {
        // 任意[一方为空，一方不为空]
        if (p == null ^ q == null) {
            return false;
        }

        if (p == null && q == null) {
            return true;
        }

        return p.val == q.val && isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
    }

    // 判断一个树是否为镜面树
    public static boolean isSymmetric(Node head) {
        return isSymmetricMirror(head, head);
    }

    private static boolean isSymmetricMirror(Node left, Node right) {
        if (null == left ^ null == right) {
            return false;
        }

        if (null == left && null == right) {
            return true;
        }

        return left.val == right.val && isSymmetricMirror(left.left, right.right) && isSymmetricMirror(left.right, right.left);
    }

    // 返回一棵树的最大深度
    public static int maxDepth(Node root) {
        if (null == root) {
            return 0;
        }
        return Math.max(maxDepth(root.left), maxDepth(root.right)) + 1;
    }

    // 用先序数组和中序数组重新构建一棵树
    public static Node buildTree(int[] pre, int[] in) {
        if (null == pre || null == in || pre.length != in.length) {
            return null;
        }
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < in.length; i++) {
            map.put(in[i], i);
        }
        return f(pre, 0, pre.length - 1, in, 0, in.length - 1, map);
    }

    // 先序[L1.R1], 中序[L2.R2]构建一棵树
    private static Node f(int[] pre, int L1, int R1, int[] in, int L2, int R2, Map<Integer, Integer> map) {
        // 蹩脚树判断
        if (L1 > R1) {
            return null;
        }
        // 先序[L1]位置 是整个树的头节点，从中序中找到先序[L1]， 即找到了头节点，左右分别为是Left和Right
        Node head = new Node(pre[L1]);
        if (L1 == R1) {
            return head;
        }

        int find = map.get(pre[L1]);
        // 2,3,4,5,6
        // 5,6,7,8,9
        head.left = f(pre, L1 + 1, L1 + find - L2, in, L2, find - 1, map);
        head.right = f(pre, L1 + find - L2 + 1, R1, in, find + 1, R2, map);
        // 然后开始重新构造
        return head;
    }

    // 二叉树 按层级收集节点 binary-tree-level-order-traversal-ii
    // * 顺序收集
    // 1. 拿到队列的size， size有多少，步骤2就进行多少回
    // 2. 弹出队列中的节点，有左先加左，有右在加右
    // -- 大数组交换/ LinkedList
    public List<List<Integer>> binaryTreeLevelOrderTraversalII(Node root) {
        List<List<Integer>> ans = new LinkedList<>();

        if (null == root) {
            return ans;
        }

        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        while (!queue.isEmpty()) {
            List<Integer> curs = new ArrayList<>();
            for (int i = 0, size = queue.size(); i < size; i++) {
                Node poll = queue.poll();
                curs.add(poll.val);
                if (null != poll.left) {
                    queue.add(poll.left);
                }
                if (null != poll.right) {
                    queue.add(poll.right);
                }
            }
            ans.add(0, curs);
        }

        return ans;
    }

    // 判断是否为平衡搜索二叉树
    // 1. 平衡树： ｜左高-右高｜ <= 1
    // 递归抓到节点的两个信息： 是否是平衡， 树的高度
    // 2. 搜索树： 左小右大
    // 中序遍历，只要是顺序数组就是搜索树
    // 递归抓到节点的信息： 是否达到搜索的条件，整个树的最大值、最小值
    // 3. 平衡搜索二叉树

    // 收集达标路径和 path-sum-ii
    public static List<List<Integer>> pathSum(Node root, int sum) {
        // ans添加copy
        // path.remove() 清理现场 防止返回上层出现脏数据
        // process(Node, List path, int preSum, int sum, List<List> ans)
        return null;
    }

    // 能够组成路径和 path-sum
    public static boolean hasPathSum(Node root, int sum) {
        // process(Node, 已经累计和，sum)
        // 需要注意为空的情况
        return false;
    }
}
