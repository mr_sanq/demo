package top.zopx.algorithm.demo001;

import java.util.Arrays;

/**
 * 排序： 快速排序
 *
 * @author Mr.Xie
 * @email xiezhyan@126.com
 * @date 2023/6/24 19:05
 */
public class Code007 {
    // 小于等于P的放左边，大于P的放右边
    // 1. 当前数 <= P 当前数和<=区的下一个数交换， <=区右扩， 当前数跳下一个
    // 2. 当前数 > P ，当前数直接跳下一个
    public static void splitLessEq(int[] arr) {
        int lessEqL = -1;
        int index = 0;
        int eq = arr.length - 1;
        while (index < arr.length) {
            if (arr[index] <= arr[eq]) {
                swap(arr, index++, ++lessEqL);
            } else {
                index++;
            }
        }
    }

    static int[] copy(int[] arr) {
        int[] copy = new int[arr.length];
        System.arraycopy(arr, 0, copy, 0, arr.length);
        return copy;
    }

    public static void main(String[] args) {
        int maxLength = 50;
        int maxVal = 50;
        for (int i = 0; i < 100_0000; i++) {
            int[] arr = buildRandom(maxLength, maxVal);
            quickSort(arr);
            int[] copy = copy(arr);
            Arrays.sort(copy);
            if (!isEquals(arr, copy)) {
                System.out.println(Arrays.toString(arr));
                break;
            }
        }
        System.out.println("OK");
    }

    private static boolean isEquals(int[] arr, int[] copy) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != copy[i]) {
                return false;
            }
        }
        return true;
    }

    private static int[] buildRandom(int maxLength, int maxVal) {
        int[] arr = new int[(int)(Math.random() * maxLength)];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int)(Math.random() * maxVal);
        }
        return arr;
    }

    public static void swap(int[] arr, int i, int minValueIndex) {
        int tmp = arr[i];
        arr[i] = arr[minValueIndex];
        arr[minValueIndex] = tmp;
    }

    // 小于P的放左边，等于P的放中间，大于P的放右边
    // <区 -1位置， >区在P位置
    // 1. 当前数 <= P 当前数和<区的下一个数交换， <区右扩， 当前数跳下一个
    // 2. 当前数>P, 当前数和>区的前一个数交换，当前数不动，>区左扩
    // 3. == index直接跳
    // 4. 当前数和>区的边界相同，>区的第一个位置和P交换
    public static void splitLessEqMore(int[] arr) {
        int lessL = -1, moreR = arr.length - 1, index = 0;
        while (index < moreR) {
            if (arr[index] < arr[arr.length - 1]) {
                swap(arr, ++lessL, index++);
            } else if (arr[index] > arr[arr.length - 1]) {
                swap(arr, --moreR, index);
            } else {
                index++;
            }
        }
        swap(arr, moreR, arr.length - 1);
    }

    // 快排
    // partition返回==区域的边界
    public static void quickSort(int[] arr) {
        if (null == arr || arr.length < 2) {
            return;
        }

        process(arr, 0, arr.length - 1);
    }

    private static void process(int[] arr, int L, int R) {
        if (L >= R) {
            return;
        }

        int[] partition = partition(arr, L, R);
        process(arr, L, partition[0] - 1);
        process(arr, partition[1], R);
    }

    private static int[] partition(int[] arr, int L, int R) {
        int index = L;
        int lessR = L - 1;
        int moreL = R;
        while (index < moreL) {
            if (arr[index] < arr[R]) {
                swap(arr, ++lessR, index++);
            } else if (arr[index] > arr[R]) {
                swap(arr, --moreL, index);
            } else {
                index++;
            }
        }
        swap(arr, moreL, R);
        return new int[]{lessR + 1, moreL};
    }

}
