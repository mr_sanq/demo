package top.zopx.algorithm.demo002;

import java.util.PriorityQueue;

/**
 * 优先级队列: PriorityQueue
 * <p>23. Merge k Sorted Lists</p>
 *
 * @author Mr.Xie
 * @email xiezhyan@126.com
 * @date 2023/6/24 19:05
 */
public class Code002 {

    // 使用双链表实现队列
    // 使用数组实现队列

    public static void main(String[] args) {
        // 小根堆
        PriorityQueue<Integer> queue = new PriorityQueue<>((o1, o2) -> o2 - o1);
        queue.add(1);
        queue.add(3);
        queue.add(2);

        System.out.println(queue.peek());

    }
}
