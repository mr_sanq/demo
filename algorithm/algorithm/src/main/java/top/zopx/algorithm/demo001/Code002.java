package top.zopx.algorithm.demo001;

import java.util.Arrays;

/**
 * @author Mr.Xie
 * @email xiezhyan@126.com
 * @date 2023/6/24 19:05
 */
public class Code002 {

    // 选择排序
    public static void selectSort(int[] arr) {
        if (null == arr || arr.length < 2) {
            return;
        }

        // 0 ~ len-1
        // 1 ~ len-1
        // 2 ~ len-1
        int len = arr.length;
        for (int i = 0; i < len; i++) {
            int minValueIndex = i;
            // 最小数据的位置
            for (int j = i + 1; j < len; j++) {
                minValueIndex = arr[j] < arr[minValueIndex] ? j : minValueIndex;
            }
            // 交换
            swap(arr, i, minValueIndex);
        }
    }

    public static void bubbleSort(int[] arr) {
        if (null == arr || arr.length < 2) {
            return;
        }

        int len = arr.length;
        // 0 ~ len-1
        // 0 ~ len-2
        // 0 ~ len-3
        // 第一层for循环取定范围
        for (int i = len - 1; i >= 0; i--) {
            // 第二层比较
            for (int j = 1; j <= i; j++) {
                // 0 1， 1 2， 2 3， 3 4  n n-1
                // 两两比较交换，将最大的移到最后
                if (arr[j - 1] > arr[j]) {
                    swap(arr, j - 1, j);
                }
            }
        }
    }

    public static void insertSort(int[] arr) {
        if (null == arr || arr.length < 2) {
            return;
        }

        // 0~0 是否有序
        // 0~1 是否有序 并且1位置和前面的数比较
        // 0~2 是否有序 并且2位置和前面的数比较
        // 0~3 是否有序 并且3位置和前面的数比较
        // 0~n 是否有序 并且n位置和前面的数比较

        // 范围
        for (int i = 1; i < arr.length; i++) {
            int curIndex = i;
            while (curIndex - 1 >= 0 && arr[curIndex - 1] > arr[curIndex]) {
                swap(arr, curIndex - 1, curIndex);
                curIndex--;
            }
        }

    }

    public static void swap(int[] arr, int i, int minValueIndex) {
        int tmp = arr[i];
        arr[i] = arr[minValueIndex];
        arr[minValueIndex] = tmp;
    }

    public static void main(String[] args) {
        int[] arr = {9, 4, 7, 2, 1, 0, 1, 2, 1, 0};
        System.out.println(Arrays.toString(arr));
        insertSort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
