package top.zopx.algorithm.demo001;

/**
 * 二分
 *
 * @author Mr.Xie
 * @email xiezhyan@126.com
 * @date 2023/6/24 19:05
 */
public class Code004 {

    // 有序数组中找到num  一直在L ～ R变化  L<=R 有效 继续while    小 L = mid + 1   大 R = mid - 1
    public static int findNum(int[] arr, int target) {
        int l = 0;
        int r = arr.length - 1;
        while (l <= r) {
            int mid = (l + r) >> 1;
//            int mid = l + ((r - l) >> 1);
            if (arr[mid] == target) {
                return mid;
            } else if (arr[mid] > target) {
                r = mid - 1;
            } else {
                l = mid + 1;
            }
        }

        return -1;
    }

    // 有序数组中找到>=num最左的位置 定义一个变量T   T最晚更新的内容就是当前答案
    public static int findThanNumLeft(int[] arr, int num) {
        int ans = -1;
        int l = 0;
        int r = arr.length - 1;
        while (l <= r) {
            int mid = (l + r) >> 1;
            if (arr[mid] >= num) {
                l = mid + 1;
                ans = mid;
            } else {
                r = mid - 1;
            }
        }
        return ans;
    }

    // 有序数组中找到<=num最右的位置
    public static int findLessNumRight(int[] arr, int num) {
        int ans = -1;
        int l = 0;
        int r = arr.length - 1;
        while (l <= r) {
            int mid = (l + r) >> 1;
            if (arr[mid] <= num) {
                r = mid - 1;
                ans = mid;
            } else {
                l = mid + 1;
            }
        }
        return ans;
    }

    // 任意两个相邻位置不等 局部最小位置 while(L < R-1) { mid < mid + 1 && mid < mid - 1 }   l < R ? l : R

    public static void main(String[] args) {
    }
}
