package top.zopx.algorithm.demo001;

/**
 * 二进制:
 *  & | ~ ^ << >> >>>
 *
 * @author Mr.Xie
 * @email xiezhyan@126.com
 * @date 2023/6/24 19:05
 */
public class Code005 {

    // 位图
    // num  >> 6  = num / 64
    // num  & 63  = num % 64

    // ^: 无进位相加: 满足交换律和结合律

    // 0 ^ N = N  N^N = 0

    // https://leetcode.cn/problems/single-number/
        // 数组中一种数出现了奇数次，其他数都出现了偶数次，怎么找到并打印这种数
    // ---
    // 把int类型数的最右侧的1提取出来 a & (-a)

    // https://leetcode.cn/problems/single-number-iii/description/
    // 两种数出现了奇数次，其他数出现了偶数次，找到这两种数
        // 第一次循环： eor = a ^ b
        // 找到eor最右侧的1 >>> 说明a和b的某一位不一样(^的特性)
        // 在数组中找到某一位上为1的数，和eor'^
        // if(arr[i] & rightOne != 0) {eor'^arr[i]}
        // 最后 eor' ^ eor 就是这两个数

    // https://leetcode.cn/problems/single-number-ii/description/
    /// 一种数出现了K次，其他数都出现了M次(M>1, K<M) 找到出现K次的数 （空间：O(1)，时间： O(N)）
        // t[32]
        // 将所有数字的二进制的1累加在t上 i=31, i>=0, i-- t[i] += ((num >> i) & 1)
        // t[i] % M != 0 这一位上出现在K次的数上 ans |= (1 << i)



    public static void main(String[] args) {
    }
}
