package top.zopx.algorithm.demo001;

/**
 *
 * 排序： 归并排序
 *
 * @author Mr.Xie【流水不争先，争的是滔滔不绝，争的是磅礴的生命力和不屈的精神】
 * @email xiezhyan@126.com
 * @date 2023/6/24 19:05
 */
public class Code006 {

    // mid = L + ((R - L) >> 1)
    public static void mergeSort(int[] arr) {
        if (null == arr || arr.length < 2) {
            return;
        }

        process(arr, 0, arr.length);
    }

    private static void process(int[] arr, int L, int R) {
        if (L == R) {
            return;
        }
        int mid = L + ((R - L) >> 1);
        process(arr, L, mid);
        process(arr, mid + 1, R);
        merge(arr, L, mid, R);
    }

    private static void merge(int[] arr, int L, int mid, int R) {
        int[] helper = new int[R - L + 1];
        int i = 0;
        int p1 = L, p2 = mid + 1;
        while (p1 <= mid && p2 <= R) {
            helper[i++] = arr[p1] <= arr[p2] ? arr[p1++] : arr[p2++];
        }

        while (p1 <= mid) {
            helper[i++] =  arr[p1++];
        }
        while (p2 <= R) {
            helper[i++] =  arr[p2++];
        }

        for (int j = 0; j < helper.length; j++) {
            arr[L + j] = helper[j];
        }
    }

    // 小和问题: 每个数左边比自己小的数进行累加，最后做累加和
        // merge: 左组小 产生小和 (i * R - p2 + 1)
        // 相等先拷贝右组

    // 逆序对： (左，右)⬇️降序称之为逆序对， 有多少逆序对
        // 从右往左merge，相等的时候先拷贝右组，找到几个比自己小， 最后累加
    // 返回「num的右边右多少个数*2后依然比自己小」的个数
        //  目前囊括进来的数从
//        int ans = 0;
//        int windowR = m + 1;
//        for (i = L; i <= m; i++) {
//            while(windowR <= R && arr[i] > (arr[windowR] << 1)) {
//                windowR++
//            }
//            ans += windowR - m - 1
//        }

    // 前缀和数组
    public static int[] preSum(int[] arr) {
        int[] resultArr = new int[arr.length];
        resultArr[0] = arr[0];
        for (int i = 1; i < arr.length; i++) {
            resultArr[i] += (resultArr[i-1] + arr[i]);
        }
        return resultArr;
    }
    // 给定一个数组arr，两个整数lower和upper， 返回arr总右多少子数组的累加和在[lower,upper]范围上
        // 每个i位置结尾的达标的数
    public static int countRangeSum(int[] nums, int lower, int upper) {

        if (nums == null || nums.length == 0) {
            return 0;
        }

        int[] sum = preSum(nums);
        return count(sum, 0, nums.length, lower, upper);
    }

    public static int count(int[] sum, int L, int R, int lower, int upper) {
        if (L == R) {
            if (sum[L] >= lower && sum[L] <= upper) {
                return 1;
            } else {
                return 0;
            }
        }

        // 范围上不止一个位置
        int mid = L + ((R - L) >> 1);
        int leftPart = count(sum, L, mid, lower, upper);
        int rightPart = count(sum, mid + 1, R, lower, upper);
        int merge = 0;
        return leftPart + rightPart + merge;
    }
}
