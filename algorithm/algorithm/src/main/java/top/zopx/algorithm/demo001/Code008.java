package top.zopx.algorithm.demo001;

import java.util.ArrayList;
import java.util.List;

/**
 * 拆分数字
 *
 * @author Mr.Xie
 * @email xiezhyan@126.com
 * @date 2023/6/24 19:05
 */
public class Code008 {

    public static void main(String[] args) {
        int num = 123;

        List<Integer> ans = new ArrayList<>();
        while (num != 0) {
            ans.add(num % 10);
            num /= 10;
        }

        int res = ans.get(0);
        for (int i = 1; i < ans.size(); i++) {
            res = res * 10 + ans.get(i);
        }
        System.out.println(res);
    }

}
