/**
 * <p>简单的初始代码</p>
 * <ul>
 *     <li>code001: 打印二进制、N的阶乘相加、获取数组中最大值与第二大值</li>
 *     <li>code002: 选择排序、冒泡排序、插入排序</li>
 *     <li>code003: 前缀和、random概率问题</li>
 *     <li>code004: 二分</li>
 *     <li>code005: 二进制运算</li>
 * </ul>
 * @author Mr.Xie
 * @email xiezhyan@126.com
 * @date 2023/6/24 08:33
 */
package top.zopx.algorithm.demo001;