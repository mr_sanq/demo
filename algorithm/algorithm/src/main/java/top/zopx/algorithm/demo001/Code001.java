package top.zopx.algorithm.demo001;

/**
 * @author Mr.Xie
 * @email xiezhyan@126.com
 * @date 2023/6/24 08:37
 */
public class Code001 {

    /**
     * 打印int类型数值的二进制标识形式
     *
     * @param num
     */
    public static void printBinary(int num) {
        // int 32位
        for (int i = 31; i >= 0; i--) {
            System.out.print(
                    (num & (1 << i)) == 0 ? "0" : "1"
            );
        }
        System.out.println();
    }

    /**
     * 1! + 2! + 3! + ... + N!
     * 1 + 1*2 + 1*2*3 + ... +
     *
     * @param n
     */
    public static void fac(int n) {
        if (n <= 0) {
            return;
        }

        // 1! = 1
        // 2! = 1! * 2
        // 3! = 2! * 3
        // N! = (N-1)! * n
        int result = 1;
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            result *= i;
            sum += result;
        }
        System.out.println(sum);
    }

    public static void findMaxOrSecondMax(int[] arr) {
        if (null == arr || arr.length <= 0) {
            return;
        }

        int max = arr[0];
        int secondMax = 0;

        if (arr.length == 1) {
            System.out.println("max = " + max);
            return;
        }

        if (arr.length == 2) {
            max = Math.max(max, arr[1]);
            secondMax = arr[0] == max ? arr[1] : arr[0];
        }

        for (int i = 1; i < arr.length; i++) {
            // max和每一个值比较
            if (max < arr[i]) {
                secondMax = max;
                max = arr[i];
            }
            // seconds和排除掉max之后的每一个值比较
            if (secondMax < arr[i] && arr[i] != max) {
                secondMax = arr[i];
            }
        }

        System.out.println("max = " + max + " second = " + secondMax);
    }

    public static void main(String[] args) {
        // do noting
    }

}
