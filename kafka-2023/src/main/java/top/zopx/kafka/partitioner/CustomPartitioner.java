package top.zopx.kafka.partitioner;

import org.apache.kafka.clients.producer.Partitioner;
import org.apache.kafka.clients.producer.internals.BuiltInPartitioner;
import org.apache.kafka.common.Cluster;

import java.util.Map;

/**
 *
 * @author 谢先生
 * @email xiezhyan@126.com
 * @date 2023/2/13 21:13
 */
public class CustomPartitioner implements Partitioner {

    @Override
    public void configure(Map<String, ?> configs) {
        // nothing
    }

    @Override
    public int partition(String topic, Object key, byte[] keyBytes, Object value, byte[] valueBytes, Cluster cluster) {
        // 如果keyBytes == null
        // 直接去0号位置
        if (null == keyBytes) {
            return 0;
        }

        // 已默认分区策略实现
        int numPartitions = cluster.partitionsForTopic(topic).size();
        return BuiltInPartitioner.partitionForKey(keyBytes, numPartitions);
    }

    @Override
    public void close() {
        // nothing
    }

}
