package top.zopx.kafka.partitioner;

import org.apache.kafka.clients.producer.*;

import java.io.IOException;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

/**
 * 分区器测试类
 * @author 谢先生
 * @email xiezhyan@126.com
 * @date 2023/1/24 23:27
 */
public class KafkaPartitionProducer {

    public static void main(String[] args) {
        Properties config = new Properties();
        // --bootstrap-server
        config.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "master:9092,node01:9092,node02:9092");
        // key 序列化器
        config.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        // value 序列化器
        config.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        // ack
        config.setProperty(ProducerConfig.ACKS_CONFIG, "all");
        // 修改分区策略类
//        config.setProperty(ProducerConfig.PARTITIONER_CLASS_CONFIG, "org.apache.kafka.clients.producer.RoundRobinPartitioner");
        // 已测试
        config.setProperty(ProducerConfig.PARTITIONER_CLASS_CONFIG, "top.zopx.kafka.partitioner.CustomPartitioner");

//        config.setProperty(ProducerConfig.BATCH_SIZE_CONFIG, "");

        try(Producer<String, String> producer = new KafkaProducer<>(config)) {
            for (int i = 0; i < 10; i++) {
                ProducerRecord<String, String> record = new ProducerRecord<>(
                        "newTopic001",
                        "key" + i,
                        "data from " + KafkaPartitionProducer.class.getName() + ", i = " + i
                );

                sync(producer, record);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void sync(Producer<String, String> producer, ProducerRecord<String, String> record) throws Exception {
        RecordMetadata recordMetadata = producer.send(record).get();
        System.out.println(
                MessageFormat.format("topic = {0}\t, partition = {1}\t, offset = {2}\t, timestamp = {3}",
                        recordMetadata.topic(),
                        recordMetadata.partition(),
                        recordMetadata.offset(),
                        recordMetadata.timestamp() + ""
                )
        );
    }
}
