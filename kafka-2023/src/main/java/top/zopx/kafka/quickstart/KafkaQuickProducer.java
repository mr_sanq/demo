package top.zopx.kafka.quickstart;

import org.apache.kafka.clients.producer.*;

import java.io.IOException;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Properties;

/**
 * @author 谢先生
 * @email xiezhyan@126.com
 * @date 2023/1/24 23:27
 */
public class KafkaQuickProducer {

    public static void main(String[] args) {
        Properties config = new Properties();
        // --bootstrap-server
        config.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "master:9092,node01:9092,node02:9092");
        // key 序列化器
        config.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        // value 序列化器
        config.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        // ack
        config.setProperty(ProducerConfig.ACKS_CONFIG, "all");
        // 开启幂等
        config.setProperty(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true");
        config.setProperty(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "5");
        config.setProperty(ProducerConfig.LINGER_MS_CONFIG, "5000");

        try(Producer<String, String> producer = new KafkaProducer<>(config)) {
            ProducerRecord<String, String> record = new ProducerRecord<>(
                    "newTopic001",
                    "key01",
                    "data from " + KafkaQuickProducer.class.getName()
            );

            sync(producer, record);

//            async(producer, record);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void async(Producer<String, String> producer, ProducerRecord<String, String> record) {
        producer.send(record, (recordMetadata, exception) -> {
            if (null != exception) {
                exception.printStackTrace();
                return;
            }

            // ack = 0
            // newTopic001	2	-1	1674870287856

            // ack = 1 # default
            // newTopic001	1	1	1674870252964
            System.out.println(
                    MessageFormat.format("{0}\t{1}\t{2}\t{3}",
                            recordMetadata.topic(),
                            recordMetadata.partition(),
                            recordMetadata.offset(),
                            recordMetadata.timestamp() + ""
                    )
            );
        });

        try {
            System.in.read();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static void sync(Producer<String, String> producer, ProducerRecord<String, String> record) throws Exception {
        System.out.println(LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
        RecordMetadata recordMetadata = producer.send(record).get();
        System.out.println(
                MessageFormat.format("{0}\t{1}\t{2}\t{3}",
                        recordMetadata.topic(),
                        recordMetadata.partition(),
                        recordMetadata.offset(),
                        recordMetadata.timestamp() + ""
                )
        );
        System.out.println(LocalDateTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss")));
    }
}
