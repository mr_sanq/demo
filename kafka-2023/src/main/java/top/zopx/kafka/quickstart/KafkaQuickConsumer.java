package top.zopx.kafka.quickstart;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.*;

import java.io.IOException;
import java.text.MessageFormat;
import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

/**
 * @author 谢先生
 * @email xiezhyan@126.com
 * @date 2023/1/24 23:27
 */
public class KafkaQuickConsumer {

    public static void main(String[] args) {
        Properties config = new Properties();
        // --bootstrap-server
        config.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "master:9092,node01:9092,node02:9092");
        // key 反序列化器
        config.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        // value 反序列化器
        config.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringDeserializer");
        // groupId
        config.setProperty(ConsumerConfig.GROUP_ID_CONFIG, KafkaQuickConsumer.class.getName());

        try(KafkaConsumer<String, String> kafkaConsumer = new KafkaConsumer<>(config)) {
            kafkaConsumer.subscribe(Collections.singleton("transaction-topic"));

            for (;;) {
                ConsumerRecords<String, String> records =
                        kafkaConsumer.poll(Duration.ofMillis(100));
                for (ConsumerRecord<String, String> record : records) {
                    System.out.printf("offset = %d, key = %s, value = %s, partition = %d%n", record.offset(), record.key(), record.value(), record.partition());
                }
            }
        }


    }
}
